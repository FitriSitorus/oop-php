<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');



$sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"

echo "Nama : $sheep->name <br>"; // "shaun"
echo "leg : $sheep->legs <br>"; // 4
echo "cold blooded :  $sheep->cold_blooded <br>"; // "no"
echo "<br> <br>";

$kodok = new Frog("buduk");
echo "Nama : $kodok->name <br>";
echo "leg : $kodok->legs <br>"; 
echo "cold blooded :  $kodok->cold_blooded <br>"; // "no"
$kodok->jump();
echo "<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Nama : $sungokong->name <br>";
echo "leg : $sungokong->legs <br>"; 
echo "cold blooded :  $sungokong->cold_blooded <br>"; // "no"
$sungokong->yell();





?>